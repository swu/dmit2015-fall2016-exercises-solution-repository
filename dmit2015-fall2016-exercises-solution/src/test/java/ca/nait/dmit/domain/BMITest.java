package ca.nait.dmit.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BMITest {
	@Test
	public void testUnderweight() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(100);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(16.1, bmiBean.bmiValue(), 0.05);
		assertEquals("underweight", bmiBean.bmiClassification().toLowerCase());
		assertEquals("increased", bmiBean.healthRisk().toLowerCase());
	}

	@Test
	public void testNormal() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(140);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(22.6, bmiBean.bmiValue(), 0.05);
		assertEquals("normal weight", bmiBean.bmiClassification().toLowerCase());
		assertEquals("least", bmiBean.healthRisk().toLowerCase());	
	}

	@Test
	public void testOverweight() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(175);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(28.2, bmiBean.bmiValue(), 0.05);
		assertEquals("overweight", bmiBean.bmiClassification().toLowerCase());
		assertEquals("increased", bmiBean.healthRisk().toLowerCase());
	}

	@Test
	public void testObeseClassI() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(200);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(32.3, bmiBean.bmiValue(), 0.05);
		assertEquals("obese class i", bmiBean.bmiClassification().toLowerCase());
		assertEquals("high", bmiBean.healthRisk().toLowerCase());
	}
	
	@Test
	public void testObeseClassII() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(230);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(37.12, bmiBean.bmiValue(), 0.005);
		assertEquals("obese class ii", bmiBean.bmiClassification().toLowerCase());
		assertEquals("very high", bmiBean.healthRisk().toLowerCase());
	}
	
	@Test
	public void testObeseClassIII() {
		BMI bmiBean = new BMI();
		bmiBean.setWeight(250);
		bmiBean.setHeight( 5 * 12 + 6);
		assertEquals(40.35, bmiBean.bmiValue(), 0.005);
		assertEquals("obese class iii", bmiBean.bmiClassification().toLowerCase());
		assertEquals("extremely high", bmiBean.healthRisk().toLowerCase());
	}

}
