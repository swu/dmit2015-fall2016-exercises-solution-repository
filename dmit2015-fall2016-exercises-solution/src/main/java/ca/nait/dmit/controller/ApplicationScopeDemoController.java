package ca.nait.dmit.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.hibernate.validator.constraints.NotBlank;

import helper.JSFHelper;

@Named
@ApplicationScoped
public class ApplicationScopeDemoController implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	@NotBlank(message="Please enter your username.")
	private String username;
	private List<String> loggedInUsers;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public List<String> getLoggedInUsers() {
		return loggedInUsers;
	}
	
	public ApplicationScopeDemoController() {
		super();
	}
	
	@PostConstruct
	public void init() {
		loggedInUsers = new ArrayList<>();
	}
	
	public void doSignIn() {
		JSFHelper.addInfoMessage(username + " has signed in.");
		// add the current user to the collection of loggedInUsers
		loggedInUsers.add(username);
		// clear the username field
		username = "";
	}
	
	public void doRemoveUser(String username) {
		loggedInUsers.remove(username);
		JSFHelper.addInfoMessage(username + " has signed out.");
	}
	
}
