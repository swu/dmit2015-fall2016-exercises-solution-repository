package ca.nait.dmit.controller;

import java.util.Random;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import helper.JSFHelper;

@Named			// mark this class as a CDI managed bean
@SessionScoped	// @SessionScoped and @ApplicationScopedmust beans
				// must implement java.io.Serializable interface
public class NumberGuessController implements java.io.Serializable {
	private static final long serialVersionUID = 1L;

	public static final int MIN_NUMBER = 0;
	public static final int MAX_NUMBER = 100;
	
	private int playerGuessNumber;		// the number submitted by the player
	private int computerNumber;			// the random number picked by the computer
	private int playerGuessCount;		// the number of guesses made by the player
	
	
	public int getPlayerGuessNumber() {
		return playerGuessNumber;
	}

	public void setPlayerGuessNumber(int playerGuessNumber) {
		this.playerGuessNumber = playerGuessNumber;
	}

	public int getPlayerGuessCount() {
		return playerGuessCount;
	}
	
	public NumberGuessController() {
		super();
	}

	// do NOT use constructors for initialization for CDI managed beans
	// use an method annotated with @PostConstruct
	@PostConstruct
	public void init() {
		// assign a random number between 0 and 100 to computerNumber
		computerNumber = new Random().nextInt(MAX_NUMBER + 1);
		playerGuessCount = 0;
		System.out.println("Computer number: " + computerNumber);
	}
	
	public void doGuess() {
		playerGuessCount++;
		if( playerGuessNumber == computerNumber ) {
			String message = String.format("Yah! You guessed the correct number in %d attempts!", playerGuessCount);
			JSFHelper.addInfoMessage(message);
			// call init() method to generated another computerNumber and reset the playerGuessCount back to zero.
			init();
		} else if( playerGuessNumber < computerNumber && playerGuessNumber >= 0 ) {
			String message = String.format("Sorry, %d isn't it. Guess a lower number.", playerGuessCount);
			JSFHelper.addWarningMessage(message);
		} else if( playerGuessNumber > computerNumber && playerGuessNumber <= 100 ) {
			String message = String.format("Sorry, %d isn't it. Guess a higher number.", playerGuessCount);
			JSFHelper.addWarningMessage(message);
		} else {
			String message = String.format("Sorry, but guess number must be between %d and %d", MIN_NUMBER, MAX_NUMBER);
			JSFHelper.addErrorMessage(message);
		}
		JSFHelper.addInfoMessage("Guess attempt #" + playerGuessCount);
	}
	
}
