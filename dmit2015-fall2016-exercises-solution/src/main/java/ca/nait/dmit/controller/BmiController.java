package ca.nait.dmit.controller;

import javax.enterprise.inject.Model;

import ca.nait.dmit.domain.BMI;
import helper.JSFHelper;

// This is the "backing bean" class for "bmi.xhtml"
// Mark this class as CDI(Context Dependency Injection) managed bean class
@Model	// alias for @Named @RequestedScoped
public class BmiController {

	private BMI bmiInstance = new BMI();
	
	public BMI getBmiInstance() {
		return bmiInstance;
	}
	public void setBmiInstance(BMI bmiInstance) {
		this.bmiInstance = bmiInstance;
	}

	// define an "event-handler" method for the JSF commandButton
	public void calculate() {	
		String message = String.format("Your BMI is %.1f, your BMI classification is %s and your risk of developing health problems is %s"
				, bmiInstance.bmiValue()
				, bmiInstance.bmiClassification()
				, bmiInstance.healthRisk());
		if( bmiInstance.bmiClassification().equalsIgnoreCase("underweight") 
				||
			bmiInstance.bmiClassification().equalsIgnoreCase("overweight") )
			JSFHelper.addWarningMessage(message);
		else if( bmiInstance.bmiClassification().equalsIgnoreCase("normal weight") )
			JSFHelper.addInfoMessage(message);
		else if( bmiInstance.bmiClassification().equalsIgnoreCase("obese class I") )
			JSFHelper.addErrorMessage(message);
		else 
			JSFHelper.addFatalMessage(message);
	}
		
}
