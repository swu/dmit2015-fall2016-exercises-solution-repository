package chinook.service;

import java.math.BigDecimal;
//import java.math.BigDecimal;
//import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chinook.domain.GenreRevenue;

@Stateful
public class GenreService {

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<GenreRevenue> genreRevenues() {		
//		List<GenreRevenue> genreRevenues = new ArrayList<>();

//		// SELECT Genre.Name, SUM(InvoiceLine.UnitPrice * InvoiceLine.Quantity) AS GenreTotalRevenue FROM Genre, Track, InvoiceLine WHERE Genre.GenreId = Track.GenreId AND Track.TrackId = InvoiceLine.TrackId GROUP BY Genre.Name ORDER BY GenreTotalRevenue DESC
//		List<Object[]> queryResults 
//			= entityManager
//				.createNativeQuery("SELECT Genre.Name, SUM(InvoiceLine.UnitPrice * InvoiceLine.Quantity) AS GenreTotalRevenue FROM Genre, Track, InvoiceLine WHERE Genre.GenreId = Track.GenreId AND Track.TrackId = InvoiceLine.TrackId GROUP BY Genre.Name ORDER BY GenreTotalRevenue DESC")
//				.getResultList();
//		for(Object[] row : queryResults) {
//			String genreName = (String) row[0];
//			BigDecimal genreTotalRevenue = (BigDecimal) row[1];
//			GenreRevenue genreRevenue = new GenreRevenue();
//			genreRevenue.setGenreName(genreName);
//			genreRevenue.setGenreTotalRevenue(genreTotalRevenue);
//			genreRevenues.add(genreRevenue);
//		}		
//		return genreRevenues;
		
//		return entityManager.createQuery(
//				"SELECT new chinook.domain.GenreRevenue(g.name, SUM(il.unitPrice * il.quantity) AS GenreTotalRevenue) "
//				+ " FROM Genre g JOIN g.tracks t JOIN t.invoiceLines il "
//				+ " GROUP BY g.name "
//				+ " ORDER BY GenreTotalRevenue"
//				).getResultList();
		
//		return entityManager.createQuery(
//				"SELECT new chinook.domain.GenreRevenue(g.name, SUM(il.unitPrice * il.quantity) AS GenreTotalRevenue) "
//				+ " FROM InvoiceLine il JOIN il.track t JOIN t.genre g"
//				+ " GROUP BY g.name "
//				+ " ORDER BY GenreTotalRevenue"
//				).getResultList();
		
		return entityManager.createQuery(
				"SELECT new chinook.domain.GenreRevenue(g.name, SUM(il.unitPrice * il.quantity) AS GenreTotalRevenue) "
				+ " FROM InvoiceLine il, IN (il.track) t, IN (t.genre) g"
				+ " GROUP BY g.name "
				+ " ORDER BY GenreTotalRevenue"
				).getResultList();

	}
	
	public BigDecimal totalRevenueByGenreId(int genreId) {
		return (BigDecimal) entityManager.createQuery("SELECT SUM(unitPrice * quantity) FROM InvoiceLine WHERE track.genre.genreId = :genreIdValue"
				).setParameter("genreIdValue", genreId)
				.getSingleResult();
	}
	
}
