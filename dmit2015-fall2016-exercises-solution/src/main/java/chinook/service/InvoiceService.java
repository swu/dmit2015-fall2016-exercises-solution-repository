package chinook.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chinook.entity.Customer;
import chinook.entity.Invoice;
import chinook.entity.InvoiceLine;

@Stateful
public class InvoiceService {
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	public BigDecimal totalOfAllInvoices() {
		return (BigDecimal) entityManager.createQuery("SELECT SUM(i.total) FROM Invoice i").getSingleResult();
	}

	public BigDecimal totalOfAllInvoicesUsingNativeQuery() {
		return (BigDecimal) entityManager.createNativeQuery("SELECT SUM(total) FROM Invoice").getSingleResult();
	}
	
	public void createNewInvoice(Customer currentCustomer, List<InvoiceLine> items) {
		// create a new Invoice entity
		Invoice currentInvoice = new Invoice();
		// set the customer, invoiceDate, and total 
		currentInvoice.setCustomer(currentCustomer);
		currentInvoice.setInvoiceDate( new Date() );
		double totalPrice = 0;
		for(InvoiceLine item : items) {
			totalPrice += item.getQuantity() * item.getUnitPrice().doubleValue();
		}
		currentInvoice.setTotal( new BigDecimal(totalPrice) );
		// persist the entity
		entityManager.persist(currentInvoice);
		
		// for each InvoiceLine set the invoice
		// persist the InvoiceLine entity
		for(InvoiceLine item : items) {
			item.setInvoice( currentInvoice );
			entityManager.persist(item);
		}
		
	}

}
