package chinook.service;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import chinook.entity.Artist;

@Stateful
public class ArtistService {
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	
	@SuppressWarnings("unchecked")
	public List<Artist> findAll() {
		Query query = entityManager.createNamedQuery("Artist.findAll");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Artist> findAllOrderByName() {
		Query query = entityManager.createQuery("FROM Artist ORDER BY name");
		return query.getResultList();
	}
	
	public Artist findOneByArtistId(int artistId) {
//		Query query = entityManager.createQuery("FROM Artist WHERE artistId = :artistIdValue");
//		query.setParameter("artistIdValue", artistId);		
//		return (Artist) query.getSingleResult();
		return entityManager.find(Artist.class, artistId);
	}

	public void add(Artist artist) {
		entityManager.persist(artist);
	}
	
	public void update(Artist artist) {
		Artist artistUpdated = entityManager.merge(artist);
		entityManager.persist(artistUpdated);
	}
	
	public void delete(Artist artist) {
		entityManager.remove(artist);
	}
	
	
}
