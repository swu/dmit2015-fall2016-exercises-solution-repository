package chinook.configuration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;

// Step 1: Define a Servlet filter known as springSecurityFilterChan that is responsible for apply security measures in a web application
@Configuration		// register this class a configuration class
@EnableWebSecurity	// register Spring web security in the application
@ComponentScan(basePackages = "chinook.configuration")
public class WebSecurityConfigurator extends WebSecurityConfigurerAdapter {	
	
	@Override
	protected void configure(AuthenticationManagerBuilder builder) throws Exception {
		builder
			.inMemoryAuthentication()
					.withUser("employee")
					.password("Password2015")
					.authorities("EMPLOYEE")
				.and()
					.withUser("customer")
					.password("Password2015")
					.authorities("CUSTOMER")
				.and()
					.withUser("admin")
					.password("Password2015")
					.authorities("EMPLOYEE","ADMIN");				
		
//		try {
//			DataSource dataSource = null;
//			Context initalContext = new InitialContext();
//			dataSource = (DataSource) initalContext.lookup("java:jboss/datasources/chinookDS");
//			initalContext.close();
//			builder
//			.jdbcAuthentication()
//				.dataSource(dataSource)
//				.usersByUsernameQuery(
//"SELECT employeeId, email, true FROM Employee WHERE employeeId = ?")
//				.authoritiesByUsernameQuery(
//"SELECT employeeId, title FROM Employee WHERE employeeId = ?");
////				.passwordEncoder( new BCryptPasswordEncoder() );
//		} catch(Exception e) {
//			System.out.println("No data source found");
//		}
		
//		try {
//			UserDetailsService ssUserDetailsService = null;
//			Context initalContext = new InitialContext();
//			ssUserDetailsService = (UserDetailsService) initalContext.lookup("java:module/SpringSecurityUserDetailsService!org.springframework.security.core.userdetails.UserDetailsService");
//			initalContext.close();
//			builder.userDetailsService(ssUserDetailsService);//.passwordEncoder( new BCryptPasswordEncoder() );
//		} catch(Exception e) {
//			System.out.println("No Spring Security UserDetails found");
//		}
		
		builder.authenticationProvider(
new ActiveDirectoryLdapAuthenticationProvider(
		"nait.ca", "ldap://metro-ds1.nait.ca")
		);
	}

	// keep Spring Security from evaluating access to resources for security concerns
	@Override
	public void configure(WebSecurity security) throws Exception {
		security.ignoring().antMatchers("/resources/**");		
	}

	// define URL patterns and how they are protected
	@Override
	protected void configure(HttpSecurity security) throws Exception {
		security
			.authorizeRequests()
				.antMatchers("/login.html","/pages/artists.xhtml","/pages/albums.xhtml","/pages/tracks.xhtml").permitAll()
				.antMatchers("/pages/chinook/findAlbum.xhtml").hasAnyAuthority("EMPLOYEE","ADMIN")
				.antMatchers("/pages/chinook/genreRevenueReport.xhtml").hasAuthority("ADMIN")
				.anyRequest().authenticated()
//				.anyRequest().permitAll()
//			.and().formLogin()
//				.loginPage("/login.xhtml").failureUrl("/login.xhtml?error")
//				.defaultSuccessUrl("/pages/index.xhtml")
//				.usernameParameter("username")
//				.passwordParameter("password")
//				.permitAll()
//			.and().logout()
//				.logoutUrl("/logout").logoutSuccessUrl("/login.xhtml?loggedOut")
//				.invalidateHttpSession(true).deleteCookies("JSESSIONID")
//				.permitAll()
			.and().httpBasic()
			.and().csrf().disable();
				
//		http
//			.authorizeRequests()
//				.antMatchers("/pages/chinook/**").authenticated()
//				.and()
//			.httpBasic();
	}
}
