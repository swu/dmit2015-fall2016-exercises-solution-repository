package chinook.exception;

public class IllegalGenreException extends Exception {
	private static final long serialVersionUID = 1L;

	public IllegalGenreException(String message) {
		super(message);
	}
	
}
