package chinook.jaxrs;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import chinook.entity.Album;
import chinook.entity.Artist;
import chinook.entity.Track;
import chinook.service.AlbumService;
import chinook.service.ArtistService;
import chinook.service.TrackService;

@Path("/webapi")
public class ChinookWebAPI {
	
	@Path("artists/{name}")
	@POST
	public void createArtist(@PathParam("name") 
		String artistName) {
		
	}
	
	@Path("artists/{id}")
	@DELETE
	public void deleteArtist(
			@PathParam("id") int artistId) {
		
	}
	
	@Path("artists/{id}/{name}")
	@PUT
	public void updateArtist(
			@PathParam("id") int artistId, 
			@PathParam("name") String artistName) {
		
	}
	
	
	@Path("artists")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Artist> findAllArtists() {
		return artistService.findAllOrderByName();
	}
	
	@Path("albums")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Album> findAllAlbums() {
		return albumService.findAllOrderByTitle();
	}
	
	@Path("albums/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Album> findAllAlbumsByArtistId(
			@PathParam("id") int artistId) {
		return albumService.findAllByArtistIdOrderByTitle(artistId);
	}
	
	@Path("tracks/{id}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Track findOneTrackByTrackId(
			@PathParam("id") int trackId) {
		return trackService.findOneByTrackId(trackId);
	}
	
	@Path("helloText")
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String helloText() {
		return "<h2>Hello World in Text from a RESTful web service</h2>";
	}
	
	@Path("helloHtml")
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String helloHtml() {
		return "<h2>Hello World in HTML from a RESTful web service</h2>";
	}

	@Inject
	private ArtistService artistService;
	
	@Inject
	private AlbumService albumService;
	
	@Inject
	private TrackService trackService;
}
