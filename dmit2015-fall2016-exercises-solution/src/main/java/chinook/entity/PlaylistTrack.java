package chinook.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PlaylistTrack database table.
 * 
 */
@Entity
@NamedQuery(name="PlaylistTrack.findAll", query="SELECT p FROM PlaylistTrack p")
public class PlaylistTrack implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PlaylistTrackPK id;

	public PlaylistTrack() {
	}

	public PlaylistTrackPK getId() {
		return this.id;
	}

	public void setId(PlaylistTrackPK id) {
		this.id = id;
	}

}