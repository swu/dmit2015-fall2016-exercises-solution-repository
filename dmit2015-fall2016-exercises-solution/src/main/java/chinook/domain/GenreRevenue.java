package chinook.domain;

import java.math.BigDecimal;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;


public class GenreRevenue {

	private String genreName;
	private BigDecimal genreTotalRevenue;
	
	public String getGenreName() {
		return genreName;
	}
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	public BigDecimal getGenreTotalRevenue() {
		return genreTotalRevenue;
	}
	public void setGenreTotalRevenue(BigDecimal genreTotalRevenue) {
		this.genreTotalRevenue = genreTotalRevenue;
	}
	
	public GenreRevenue() {
		super();
	}
	public GenreRevenue(String genreName, BigDecimal genreTotalRevenue) {
		super();
		this.genreName = genreName;
		this.genreTotalRevenue = genreTotalRevenue;
	}

}
