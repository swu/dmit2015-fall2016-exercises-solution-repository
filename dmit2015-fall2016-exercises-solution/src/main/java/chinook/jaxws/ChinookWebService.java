package chinook.jaxws;

import java.util.List;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import chinook.entity.Album;
import chinook.entity.Artist;
import chinook.entity.Track;
import chinook.service.AlbumService;
import chinook.service.ArtistService;
import chinook.service.TrackService;

@WebService
public class ChinookWebService {
	
	@Inject
	private ArtistService artistService;
	
	@Inject
	private AlbumService albumService;
	
	@Inject
	private TrackService trackService;
	
	@WebMethod
	public List<Artist> findAllArtists() {
		return artistService.findAllOrderByName();
	}
	
	@WebMethod
	public List<Album> findAllAlbums() {
		return albumService.findAllOrderByTitle();
	}
	
	@WebMethod
	public List<Album> findAllAlbumsByArtistId(int artistId) {
		return albumService.findAllByArtistIdOrderByTitle(artistId);
	}
	
	@WebMethod
	public Track findOneTrackByTrackId(int trackId) {
		return trackService.findOneByTrackId(trackId);
	}

}











