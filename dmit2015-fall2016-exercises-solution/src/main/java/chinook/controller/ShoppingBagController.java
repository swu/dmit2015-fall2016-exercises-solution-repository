package chinook.controller;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import chinook.entity.InvoiceLine;
import chinook.entity.Track;
import chinook.service.TrackService;

@Named
@SessionScoped
public class ShoppingBagController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<InvoiceLine> items = new ArrayList<>();	// getter
	
	@Inject
	private TrackService trackService;
	
	public String addItem() {
		FacesContext fc = FacesContext.getCurrentInstance();
		Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
		String trackIdParam = params.get("trackId");
		if( trackIdParam != null && !trackIdParam.isEmpty() ) {
			int trackId = Integer.parseInt(trackIdParam);
			Track currentTrack = trackService.findOneByTrackId(trackId);
			if( currentTrack != null ) {
				addItem(currentTrack);
			}
		}
		return "shoppingBag.xhtml?faces-redirect=true";
	}
	
	public String addItem(Track currentTrack) {
		InvoiceLine item = new InvoiceLine();
		item.setTrack(currentTrack);
		item.setQuantity(1);
		item.setUnitPrice( currentTrack.getUnitPrice() );
		// add item to shopping bag
		items.add(item);
		// return navigation to the page shoppingBag.xhtml
		return "shoppingBag.xhtml?faces-redirect=true";
	}

	public void removeItem(InvoiceLine item) {
		items.remove(item);
	}
	
	public void emptyBag() {
		items.clear();
	}

	public List<InvoiceLine> getItems() {
		return items;
	}
	
	public String getTotalPrice() {
		double totalPrice = 0;
		for(InvoiceLine item : items) {
			totalPrice += item.getQuantity() * item.getUnitPrice().doubleValue();
		}
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		return formatter.format(totalPrice);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
