package chinook.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import chinook.entity.Album;
import chinook.entity.Track;
import chinook.service.AlbumService;
import chinook.service.TrackService;
import helper.JSFHelper;

@Named
@ViewScoped
public class TrackByAlbumController implements Serializable{
	private static final long serialVersionUID = 1L;

	@Inject
	private TrackService trackService;
	
	@Inject
	private AlbumService albumService;
	
	private int albumId;
	
	private List<Track> tracksByAlbum;
	
	private Album selectedAlbum;
	
	public int getAlbumId() {
		return albumId;
	}
	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}
	public List<Track> getTracksByAlbum() {
		return tracksByAlbum;
	}	
	public Album getSelectedAlbum() {
		return selectedAlbum;
	}
	
	
	public void findTracksByAlbumid() throws IOException {
		if (albumId <= 0) {
			JSFHelper.addErrorMessage("Bad request. Please use a link from within the system.");
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			FacesContext.getCurrentInstance().responseComplete();
			ec.redirect(ec.getRequestContextPath() + "/pages/chinook/albums.xhtml");
		} else {
			selectedAlbum = albumService.findOneByAlbumId(albumId);
			if (selectedAlbum == null) {
				String message = String.format("Bad request. Unknown artistId %d", albumId);
				JSFHelper.addErrorMessage(message);
			} else {
				tracksByAlbum = trackService.findAllByAlbumIdOrderByName(albumId);
				if( tracksByAlbum == null ) {
					String message = String.format("Bad request. Unknown artistId %d", albumId);
					JSFHelper.addErrorMessage(message);
				} else if( tracksByAlbum == null || tracksByAlbum.size() == 0 ) {
					String message = String.format("There are no albums for artistId %d", albumId);
					JSFHelper.addInfoMessage(message);
				}
			}
			
		}
	}
}
