package chinook.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import chinook.entity.Track;
import chinook.exception.IllegalGenreException;
import chinook.service.PlaylistService;
import chinook.service.TrackService;
import helper.JSFHelper;

@Named
@SessionScoped
public class CreatePlaylistController implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotBlank(message="Playlist Name value is required")
	private String playlistName;					// getter/setter
	@NotNull(message="Track Id value is required")
	private Integer trackId;						// getter/setter	
	private List<Track> tracks = new ArrayList<>();	// getter
	
	@Inject
	private TrackService trackService;
	
	@Inject
	private PlaylistService playlistService;
	
	public void addTrackToPlaylist() {
		// find a Track with the given trackId
		Track currentTrack = trackService.findOneByTrackId(trackId);
		if (currentTrack != null) {
			// add the currentTrack to the list of tracks
			tracks.add(currentTrack);
			JSFHelper.addInfoMessage("Successfully added track");
			trackId = null;
		} else {
			JSFHelper.addErrorMessage("No track found.");
		}
	}
	
	public void removeTrack(Track currentTrack) {
		tracks.remove(currentTrack);
	}
	
	public void createNewPlaylist() {
		try {
			playlistService.createNew(playlistName, tracks);
			JSFHelper.addInfoMessage("Successfully created new playlist");
			playlistName = null;
			tracks.clear();
		} catch (IllegalGenreException e) {
			JSFHelper.addErrorMessage(e.getMessage());
		}
	}

	public String getPlaylistName() {
		return playlistName;
	}

	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	public Integer getTrackId() {
		return trackId;
	}

	public void setTrackId(Integer trackId) {
		this.trackId = trackId;
	}

	public List<Track> getTracks() {
		return tracks;
	}
	
}
