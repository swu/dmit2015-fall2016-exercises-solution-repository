package chinook.controller;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import chinook.entity.Album;
import chinook.service.AlbumService;

@Model
public class AlbumController {

	@Inject
	private AlbumService albumService;
	
	public List<Album> retrieveAllAlbums() {
		return albumService.findAll();
	}
}
