package chinook.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import chinook.entity.Album;
import chinook.entity.Artist;
import chinook.entity.Track;
import chinook.service.AlbumService;
import chinook.service.ArtistService;
import chinook.service.TrackService;

@Named
@ViewScoped
public class ArtistSinglePageController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private ArtistService artistService;
	
	@Inject
	private AlbumService albumService;
	
	@Inject
	private TrackService trackService;
	
	private List<Artist> allArtist;
	
	private List<Album> albumsByArtist;
	
	private List<Track> tracksFromAlbum;
	
	private Artist selectedArtist;
	
	private Album selectedAlbum;

	public Artist getSelectedArtist() {
		return selectedArtist;
	}

	public void setSelectedArtist(Artist selectedArtist) {
		this.selectedArtist = selectedArtist;
	}

	public Album getSelectedAlbum() {
		return selectedAlbum;
	}

	public void setSelectedAlbum(Album selectedAlbum) {
		this.selectedAlbum = selectedAlbum;
	}

	public List<Artist> getAllArtist() {
		return allArtist;
	}

	public List<Album> getAlbumsByArtist() {
		return albumsByArtist;
	}

	public List<Track> getTracksFromAlbum() {
		return tracksFromAlbum;
	}
	
	@PostConstruct
	public void init() {
		allArtist = artistService.findAllOrderByName();
	}
	
	public void findAlbumByArtist(Artist currentArtist) {
		selectedArtist = currentArtist;
		albumsByArtist = albumService.findAllByArtistIdOrderByTitle( selectedArtist.getArtistId() );
		tracksFromAlbum = null;
	}
	
	public void findTracksFromAlbum(Album currentAlbum) {
		selectedAlbum = currentAlbum;
		tracksFromAlbum = trackService.findAllByAlbumIdOrderByName( selectedAlbum.getAlbumId() );
	}
	
	public void cancel() {
		selectedArtist = null;
		albumsByArtist = null;
		selectedArtist = null;
		selectedAlbum = null;
	}
	
}
