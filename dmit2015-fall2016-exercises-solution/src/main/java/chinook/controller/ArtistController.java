package chinook.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import chinook.entity.Artist;
import chinook.service.ArtistService;

@Named
@ViewScoped
public class ArtistController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ArtistService artistService;
		
	public List<Artist> retreiveAllArtists() {
		return artistService.findAllOrderByName();
	}
	
}
